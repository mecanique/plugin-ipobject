/*
© Quentin VIGNAUD, 2014

Licensed under the EUPL, Version 1.1 only.
You may not use this work except in compliance with the
Licence.
You may obtain a copy of the Licence at:

http://joinup.ec.europa.eu/software/page/eupl/licence-eupl — 22 languages available

Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an “AS IS” basis,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
See the Licence for the specific language governing
permissions and limitations under the Licence.
*/

#include "IPObject.h"

IPObject::IPObject(QString Name, Project* const Project) : Object(Name, QString("IP"), Project)
{
	m_connected = false;
	socket = new QTcpSocket(this);
	connect(socket, SIGNAL(connected()), SLOT(sendConnectSignal()));
	connect(socket, SIGNAL(readyRead()), SLOT(readFunction()));
	connect(socket, SIGNAL(disconnected()), SLOT(connectionFinished()));
	
	changeStatus(Object::Error);
	changeInfos(tr("Not connected."));
}

IPObject::~IPObject()
{
	socket->close();
}

void IPObject::more()
{
	QDialog *tempDialog = new QDialog();
		tempDialog->setWindowTitle(tr("Connection to \"%1\"").arg(name()));
	QLineEdit *tempAddress = new QLineEdit(tempDialog);
	QSpinBox *tempPort = new QSpinBox(tempDialog);
		tempPort->setRange(1, 65535);
		tempPort->setValue(42000);
	QPushButton *buttonConnection = new QPushButton(tr("Connection"), tempDialog);
		connect(buttonConnection, SIGNAL(clicked(bool)), tempDialog, SLOT(accept()));
	QPushButton *buttonCancel = new QPushButton(tr("Cancel"), tempDialog);
		connect(buttonCancel, SIGNAL(clicked(bool)), tempDialog, SLOT(reject()));	
	
	QFormLayout *tempLayout = new QFormLayout(tempDialog);
		tempLayout->addRow(tr("Address of \"%1\" (IP or hostname)").arg(name()), tempAddress);
		tempLayout->addRow(tr("Connection port"), tempPort);
	QHBoxLayout *tempButtonsLayout = new QHBoxLayout();
		tempButtonsLayout->addWidget(buttonConnection);
		tempButtonsLayout->addWidget(buttonCancel);
		tempLayout->addRow(tempButtonsLayout);
	
	int Ok = tempDialog->exec();
	
	if (Ok == QDialog::Accepted)
	{
		tryConnection(tempAddress->text(), tempPort->value());
	}
	
	delete tempDialog;
}

QVariant IPObject::settings()
{
	QHash<QString, QVariant> tempSettings;
	
	tempSettings.insert("address", QVariant(address()));
	tempSettings.insert("port", QVariant(socket->peerPort()));
	
	return QVariant(tempSettings);
}

void IPObject::setSettings(QVariant Settings)
{
	QHash<QString, QVariant> tempSettings(Settings.toHash());
	tryConnection(tempSettings.value("address").toString(), tempSettings.value("port").toUInt());
}

QString IPObject::address() const
{
	return (socket->peerName().isEmpty()) ? socket->peerAddress().toString() : socket->peerName();
}

bool IPObject::isConnected() const
{
	return m_connected;
}

void IPObject::prepareFunction(char Code)
{
	if (!m_connected) return;
	arrayToSend.clear();
	arrayToSend.append(Code);
}

void IPObject::prepareBool(bool Value)
{
	if (!m_connected) return;
	if (Value) arrayToSend.append((char)0x01);
	else arrayToSend.append((char)0x00);
}

void IPObject::prepareInt(int Value)
{
	if (!m_connected) return;
	qint16 tValue = Value;
	qint16 tFirstValue = tValue;
	qint16 tSecondValue = tValue;
	
	char firstByte = tFirstValue >> 8;
	char secondByte = tSecondValue & 0xFF;
	
	arrayToSend.append(firstByte);
	arrayToSend.append(secondByte);
}

void IPObject::prepareUInt(unsigned int Value)
{
	if (!m_connected) return;
	quint16 tValue = Value;
	quint16 tFirstValue = tValue;
	quint16 tSecondValue = tValue;
	
	char firstByte = tFirstValue >> 8;
	char secondByte = tSecondValue & 0xFF;
	
	arrayToSend.append(firstByte);
	arrayToSend.append(secondByte);
}

void IPObject::send()
{
	if (!m_connected) return;
	socket->write(arrayToSend);
	QString tempLogMessage("Message send:");
	for (int i=0 ; i < arrayToSend.size() ; i++) tempLogMessage += " 0x" + QString::number(int(arrayToSend.at(i)) & 0xFF, 16);
	project()->log()->write(name(), tempLogMessage);
}

bool IPObject::takeBool()
{
	QByteArray tempValue = socket->read(1);
	if (tempValue.size() != 1) return false;
	
	if (tempValue.at(0) == 0) return false;
	else return true;
}

int IPObject::takeInt()
{
	QByteArray tempValue = socket->read(2);
	if (tempValue.size() != 2) return 0;
	
	qint16 tempInt = 0;
	tempInt = ((qint16)tempValue.at(0) & 0xFF) << 8;
	tempInt = tempInt | ((qint16)tempValue.at(1) & 0xFF);
	return tempInt;
}

unsigned int IPObject::takeUInt()
{
	QByteArray tempValue = socket->read(2);
	if (tempValue.size() != 2) return 0;
	
	quint16 tempInt = 0;
	tempInt = ((quint16)tempValue.at(0) & 0xFF) << 8;
	tempInt = tempInt | ((quint16)tempValue.at(1) & 0xFF);
	return tempInt;
}

void IPObject::tryConnection(QString Address, quint16 Port)
{
	m_connected = false;
	socket->close();
	socket->connectToHost(Address, Port);

	changeStatus(Object::Unknown);
	changeInfos(tr("Try connection to \"%1\" port \"%2\"...").arg(Address).arg(QString::number(Port)));
}

void IPObject::connectionSuccess()
{
	m_connected = true;
	project()->log()->write(name(), QString("Connected to \"%1\" port \"%2\".").arg(address()).arg(QString::number(socket->peerPort())));
	changeStatus(Object::Operational);
	changeInfos(tr("Connected to \"%1\" port \"%2\".").arg(address()).arg(QString::number(socket->peerPort())));
}

void IPObject::connectionFinished()
{
	m_connected = false;
	project()->log()->write(name(), QString("Disconnected to \"%1\".").arg(address()));
	changeStatus(Object::Error);
	changeInfos(tr("Disconnected to \"%1\".").arg(address()));
}


void IPObject::sendConnectSignal()
{
	socket->write(QByteArray(1, 0xFF));
}

void IPObject::readFunction()
{
	project()->log()->write(name(), "Data received, content: " + QString(socket->peek(socket->bytesAvailable()).toHex()) + " (" + QString::number(socket->bytesAvailable()) + " bytes)");
	if (isConnected())
		{
		bool codeFind = false;
		do
			{
			QByteArray tempValue = socket->peek(1);
			if (tempValue.size() == 1)
				{
				char tempFind = tempValue.at(0);
				codeFind = true;
		
				signalReceived(tempCode);
				}
			else codeFind = false;
			}
		while (codeFind);
		}
	else
		{
		QByteArray tempValue = socket->peek(identifier().size());
		
		if (tempValue == identifier().toLatin1()) {
			socket->read(identifier().size());
			connectionSuccess();
			}
		}
}


