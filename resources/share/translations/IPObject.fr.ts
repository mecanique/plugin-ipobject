<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="fr_FR">
<context>
    <name>IPObject</name>
    <message>
        <location filename="../../src/IPObject/IPObject.cpp" line="31"/>
        <source>Not connected.</source>
        <translation>Non connecté.</translation>
    </message>
    <message>
        <location filename="../../src/IPObject/IPObject.cpp" line="42"/>
        <source>Connection to &quot;%1&quot;</source>
        <translation>Connexion à « %1 »</translation>
    </message>
    <message>
        <location filename="../../src/IPObject/IPObject.cpp" line="47"/>
        <source>Connection</source>
        <translation>Connexion</translation>
    </message>
    <message>
        <location filename="../../src/IPObject/IPObject.cpp" line="49"/>
        <source>Cancel</source>
        <translation>Annuler</translation>
    </message>
    <message>
        <location filename="../../src/IPObject/IPObject.cpp" line="53"/>
        <source>Address of &quot;%1&quot; (IP or hostname)</source>
        <translation>Adresse de « %1 » (IP ou nom d&apos;hôte)</translation>
    </message>
    <message>
        <location filename="../../src/IPObject/IPObject.cpp" line="54"/>
        <source>Connection port</source>
        <translation>Port de connexion</translation>
    </message>
    <message>
        <location filename="../../src/IPObject/IPObject.cpp" line="185"/>
        <source>Try connection to &quot;%1&quot; port &quot;%2&quot;...</source>
        <translation>Tentative de connexion à « %1 » port « %2 »…</translation>
    </message>
    <message>
        <location filename="../../src/IPObject/IPObject.cpp" line="193"/>
        <source>Connected to &quot;%1&quot; port &quot;%2&quot;.</source>
        <translation>Connecté à « %1 » port « %2 ».</translation>
    </message>
    <message>
        <location filename="../../src/IPObject/IPObject.cpp" line="201"/>
        <source>Disconnected to &quot;%1&quot;.</source>
        <translation>Déconnecté de « %1 ».</translation>
    </message>
</context>
</TS>
