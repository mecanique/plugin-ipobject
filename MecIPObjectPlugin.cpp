/*
© Quentin VIGNAUD, 2014

Licensed under the EUPL, Version 1.1 only.
You may not use this work except in compliance with the
Licence.
You may obtain a copy of the Licence at:

http://joinup.ec.europa.eu/software/page/eupl/licence-eupl — 22 languages available

Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an “AS IS” basis,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
See the Licence for the specific language governing
permissions and limitations under the Licence.
*/

#include "MecIPObjectPlugin.h"

MecIPObjectPlugin::MecIPObjectPlugin() : MecPlugin(QString("ipobject"), __IPOBJECT_VERSION__, MecAbstractElement::Object, QString("IP"), QString("IP object"))
{
}

MecIPObjectPlugin::~MecIPObjectPlugin()
{
}

QString MecIPObjectPlugin::description() const
{
return QString(tr("Communicate with an object using the network."));
}

QString MecIPObjectPlugin::copyright() const
{
return QString("Copyright © 2014 – 2015 Quentin VIGNAUD");
}

QString MecIPObjectPlugin::developpers() const
{
return QString("Quentin VIGNAUD <quentin.vignaud@mecanique.cc>\n");
}

QString MecIPObjectPlugin::documentalists() const
{
return QString("Quentin VIGNAUD <quentin.vignaud@mecanique.cc>\n");
}

QString MecIPObjectPlugin::translators() const
{
return QString("Quentin VIGNAUD <quentin.vignaud@mecanique.cc>\n");
}

MecAbstractElementEditor* MecIPObjectPlugin::elementEditor(MecAbstractElement* const Element, MecAbstractEditor* MainEditor)
{
if (Element->elementRole() == MecAbstractElement::Object)
	{
	MecIPObjectEditor *tempEditor = new MecIPObjectEditor(static_cast<MecAbstractObject*>(Element), MainEditor);
	tempEditor->childListElementsChanged(Element);
	return tempEditor;
	}
else
	{
	return 0;
	}
}

MecAbstractElementCompiler* MecIPObjectPlugin::elementCompiler(MecAbstractElement* const Element, MecAbstractCompiler* const MainCompiler)
{
if (Element->elementRole() == MecAbstractElement::Object)
	{
	MecIPObjectCompiler *tempCompiler = new MecIPObjectCompiler(static_cast<MecAbstractObject*>(Element), MainCompiler);
	return tempCompiler;
	}
else
	{
	return 0;
	}
}

