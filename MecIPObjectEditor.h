/*
© Quentin VIGNAUD, 2014

Licensed under the EUPL, Version 1.1 only.
You may not use this work except in compliance with the
Licence.
You may obtain a copy of the Licence at:

http://joinup.ec.europa.eu/software/page/eupl/licence-eupl — 22 languages available

Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an “AS IS” basis,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
See the Licence for the specific language governing
permissions and limitations under the Licence.
*/

#ifndef __MECIPOBJECTEDITOR_H__
#define __MECIPOBJECTEDITOR_H__

#include <MecObjectEditor.h>
#include <QCryptographicHash>
#include "MecIPFunctionEditor.h"
#include "MecIPSpecialFunctionEditor.h"
#include "MecIPSignalEditor.h"

/**
\brief	Classe d'édition d'objet de type « IP ».

Les objets de type « IP » ne peuvent avoir plus de 254 fonctions et signaux, ceci en raison du protocole de communication utilisé.
*/
class MecIPObjectEditor : public MecObjectEditor
{
	Q_OBJECT

public:
	/**
	\brief	Constructeur.
	\param	Object	Objet édité, doit absolument exister lors de la construction (c.à.d. instancié et différent de 0) sinon un comportement inattendu pourrait se produire.
	*/
	MecIPObjectEditor(MecAbstractObject* const Object, MecAbstractEditor* MainEditor, QWidget * Parent=0, Qt::WindowFlags F=0);
	/**
	\brief	Destructeur.
	*/
	~MecIPObjectEditor();

	/**
	\brief	Indique si une MecFunction peut être ajouté.
	\return	true si l'objet a strictement moins de 254 fonctions et signaux (fonctions « address », « identifier » et « isConnected » non comptées), sinon false.
	*/
	bool canAddFunction() const;
	/**
	\brief	Indique si un MecSignal peut être ajouté.
	\return	true si l'objet a strictement moins de 254 fonctions et signaux (fonctions « address », « identifier » et « isConnected » non comptées), sinon false.
	*/
	bool canAddSignal() const;
	///Indique si une MecVariable peut être ajouté, retourne donc \e false.
	bool canAddVariable() const;
	///Indique si un élément enfant peut être supprimé, retourne donc \e true sauf s'il s'agit des fonctions « address », « identifier » ou « isConnected ».
	bool canRemoveChild(MecAbstractElement* const Element) const;

public slots:
	/**
	\brief	Demande un sous-éditeur pour \e Element.
	\note	\e Element doit être un élément enfant de element(), et être une fonction ou un signal, sinon 0 est retourné.
	\return	Le sous-éditeur demandé, ou 0 si éditer cet élément n'est pas possible.
	*/
	MecAbstractElementEditor* newSubEditor(MecAbstractElement *Element);

	/**
	\brief	Ajoute une fonction à l'objet IP.

	La fonction sera soumise aux contraintes des types supportés par le protocole de communication, son type de retour sera nécessairement « void ».
	*/
	void addFunction();

	/**
	\brief	Ajoute un signal à l'objet IP.

	Le signal sera soumis aux contraintes des types supportés par le protocole de communication.
	*/
	void addSignal();

	///Ne fait rien.
	void addVariable();

	/**
	\brief	Place le code à intégrer dans le programme d'une Arduino dans le presse-papiers.
	*/
	void copyArduinoCode();

	/**
	\brief	Retourne le code à intégrer dans le programme d'une Arduino pour communiquer avec elle, selon les caractéristique de l'objet.
	*/
	QString arduinoCode();

signals:


private:
	/**
	\brief	Bouton de copie du code embarqué pour différents systèmes.
	*/
	QPushButton *pushButtonCode;
	/**
	\brief	Menu des propositions des systèmes pour générer le code embarqué.
	*/
	QMenu *menuCode;
	/**
	\brief	Action correspondant à « copyArduinoCode() ».
	*/
	QAction *actionCodeArduino;


};

#endif /* __MECIPOBJECTEDITOR_H__ */

