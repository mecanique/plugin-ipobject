######################################################################
# Project file implemented for qmake (version 3 with Qt 5)
######################################################################

include(../plugin/plugin.pri)
TARGET = ipobject
DEPENDPATH += .
INCLUDEPATH += .
RESOURCES += resources.qrc
TRANSLATIONS = translations/ipobject.fr.ts

OTHER_FILES += metadata.json

HEADERS += \
	MecIPFunctionEditor.h \
	MecIPSpecialFunctionEditor.h \
	MecIPObjectCompiler.h \
	MecIPObjectEditor.h \
	MecIPObjectPlugin.h \
	MecIPSignalEditor.h  \
	MecIPVariableEditor.h

SOURCES += \
	MecIPFunctionEditor.cpp \
	MecIPSpecialFunctionEditor.cpp \
	MecIPObjectCompiler.cpp \
	MecIPObjectEditor.cpp \
	MecIPObjectPlugin.cpp \
	MecIPSignalEditor.cpp \
	MecIPVariableEditor.cpp

